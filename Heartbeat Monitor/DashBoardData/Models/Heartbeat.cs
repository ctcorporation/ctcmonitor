﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DashBoardData.Models
{
    [Table("Heartbeat")]
    public class Heartbeat
    {

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid HB_ID { get; set; }

        public DateTime HB_LastCommunicationDate { get; set; }

        public DateTime HB_LastOperation { get; set; }
        public ICollection<CTCApplication> Application { get; set; }
        public ICollection<Customer> Customer { get; set; }

    }
}
