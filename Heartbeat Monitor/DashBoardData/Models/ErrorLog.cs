﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DashBoardData.Models
{
    [Table("ErrogLog")]
    public class ErrorLog
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid ER_ID { get; set; }
        public ICollection<Customer> ER_CU { get; set; }
        public ICollection<CTCApplication> ER_AP { get; set; }
        [MaxLength(100)]
        public string ER_Process { get; set; }
        public DateTime ER_DateTime { get; set; }
        public string ER_ErrorMessage { get; set; }

    }
}
