using System.Data.Entity;

namespace DashBoardData.Models
{
    public partial class DashboardContext : DbContext
    {
        public DashboardContext()
            : base("name=DashboardContext")
        {
        }

        public DashboardContext(string connString)
            : base(connString)
        {

        }

        public virtual DbSet<Customer> Customers { get; set; }
        public virtual DbSet<CTCApplication> Applications { get; set; }
        public virtual DbSet<Heartbeat> Heartbeats { get; set; }
        public virtual DbSet<ErrorLog> ErrorLogs { get; set; }


        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
        }
    }
}
