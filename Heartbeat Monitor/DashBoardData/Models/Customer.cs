﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DashBoardData.Models
{
    [Table("Customer")]
    public class Customer
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid CU_ID { get; set; }
        [Display(Name = "Customer Name")]
        [MaxLength(100)]
        public string CU_Name { get; set; }
        public bool CU_IsActive { get; set; }
        public bool CU_OnHold { get; set; }
        public bool CU_Trial { get; set; }
        public DateTime CU_TrialStart { get; set; }
        public DateTime CU_TrialEnd { get; set; }
        public DateTime CU_LastInvoice { get; set; }
        public Guid HB_ID { get; set; }
        public Heartbeat Heartbeat { get; set; }
        [MaxLength(100)]
        public string CU_EmailAddress { get; set; }

        public Guid ER_ID { get; set; }
        public ErrorLog ErrorLog { get; set; }




    }
}
