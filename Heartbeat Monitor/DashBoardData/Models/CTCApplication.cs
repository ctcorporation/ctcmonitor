﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DashBoardData.Models
{
    [Table("Applications")]
    public class CTCApplication
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid AP_ID { get; set; }
        [MaxLength(50)]
        public string AP_Name { get; set; }
        public string AP_Code { get; set; }
        public bool AP_IsPublished { get; set; }
        public bool AP_InMaintenance { get; set; }
        public string AP_CurrentVersion { get; set; }
        public string AP_MinimumVersion { get; set; }
        public bool AP_ForceUpdate { get; set; }
        public string AP_ConnectionString { get; set; }

        public Guid HB_ID { get; set; }
        public Heartbeat Heartbeat { get; set; }

        public Guid ER_ID { get; set; }
        public ErrorLog ErrorLog { get; set; }

    }
}
