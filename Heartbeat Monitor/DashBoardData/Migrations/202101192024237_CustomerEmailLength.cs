﻿namespace DashBoardData.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class CustomerEmailLength : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Customer", "CU_EmailAddress", c => c.String(maxLength: 100));
            AlterColumn("dbo.Customer", "CU_ID", c => c.Guid(nullable: false, identity: true, defaultValueSql: "newid()"));
        }

        public override void Down()
        {
            AlterColumn("dbo.Customer", "CU_EmailAddress", c => c.String());
        }
    }
}
