﻿namespace DashBoardData.Migrations
{
    using System.Data.Entity.Migrations;


    public partial class CTCApplications : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Applications",
                c => new
                {
                    AP_ID = c.Guid(nullable: false, identity: true, defaultValueSql: "newid()"),
                    AP_Name = c.String(maxLength: 50),
                    AP_Code = c.String(),
                    AP_IsPublished = c.Boolean(nullable: false),
                    AP_InMaintenance = c.Boolean(nullable: false),
                    AP_CurrentVersion = c.String(),
                    AP_MinimumVersion = c.String(),
                    AP_ForceUpdate = c.Boolean(nullable: false),
                    AP_ConnectionString = c.String(),
                })
                .PrimaryKey(t => t.AP_ID);

        }

        public override void Down()
        {
            DropTable("dbo.Applications");
        }
    }
}
