﻿namespace DashBoardData.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class ErrorLogs : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ErrogLog",
                c => new
                {
                    ER_ID = c.Guid(nullable: false, identity: true, defaultValueSql: "newid()"),
                    ER_Process = c.String(maxLength: 100),
                    ER_DateTime = c.DateTime(nullable: false),
                    ER_ErrorMessage = c.String(),
                })
                .PrimaryKey(t => t.ER_ID);

            AddColumn("dbo.Applications", "ER_ID", c => c.Guid(nullable: false));
            AddColumn("dbo.Customer", "ER_ID", c => c.Guid(nullable: false));
            CreateIndex("dbo.Applications", "ER_ID");
            CreateIndex("dbo.Customer", "ER_ID");
            AddForeignKey("dbo.Applications", "ER_ID", "dbo.ErrogLog", "ER_ID", cascadeDelete: true);
            AddForeignKey("dbo.Customer", "ER_ID", "dbo.ErrogLog", "ER_ID", cascadeDelete: true);
        }

        public override void Down()
        {
            DropForeignKey("dbo.Customer", "ER_ID", "dbo.ErrogLog");
            DropForeignKey("dbo.Applications", "ER_ID", "dbo.ErrogLog");
            DropIndex("dbo.Customer", new[] { "ER_ID" });
            DropIndex("dbo.Applications", new[] { "ER_ID" });
            DropColumn("dbo.Customer", "ER_ID");
            DropColumn("dbo.Applications", "ER_ID");
            DropTable("dbo.ErrogLog");
        }
    }
}
