﻿// <auto-generated />
namespace DashBoardData.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.4.4")]
    public sealed partial class CTCApplications : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(CTCApplications));
        
        string IMigrationMetadata.Id
        {
            get { return "202101182105160_CTCApplications"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
