﻿namespace DashBoardData.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class heartbeatRemovecolumns : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.Heartbeat", "HB_CU");
            DropColumn("dbo.Heartbeat", "HB_AP");
           
        }
        
        public override void Down()
        {
            AddColumn("dbo.Heartbeat", "HB_AP", c => c.Guid(nullable: false));
            AddColumn("dbo.Heartbeat", "HB_CU", c => c.Guid(nullable: false));
        }
    }
}
