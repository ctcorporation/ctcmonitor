﻿namespace DashBoardData.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CustomerHeartBeat : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Customer", "HB_ID", c => c.Guid(nullable: false));
            CreateIndex("dbo.Customer", "HB_ID");
            AddForeignKey("dbo.Customer", "HB_ID", "dbo.Heartbeat", "HB_ID", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Customer", "HB_ID", "dbo.Heartbeat");
            DropIndex("dbo.Customer", new[] { "HB_ID" });
            DropColumn("dbo.Customer", "HB_ID");
        }
    }
}
