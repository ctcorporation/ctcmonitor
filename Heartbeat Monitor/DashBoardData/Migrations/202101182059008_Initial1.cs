﻿namespace DashBoardData.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class Initial1 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Customer",
                c => new
                {
                    CU_ID = c.Guid(nullable: false, identity: true, defaultValueSql: "newid()"),
                    CU_Name = c.String(maxLength: 100),
                    CU_IsActive = c.Boolean(nullable: false),
                    CU_OnHold = c.Boolean(nullable: false),
                    CU_Trial = c.Boolean(nullable: false),
                    CU_TrialStart = c.DateTime(nullable: false),
                    CU_TrialEnd = c.DateTime(nullable: false),
                    CU_LastInvoice = c.DateTime(nullable: false),
                })
                .PrimaryKey(t => t.CU_ID);

        }

        public override void Down()
        {
            DropTable("dbo.Customer");
        }
    }
}
