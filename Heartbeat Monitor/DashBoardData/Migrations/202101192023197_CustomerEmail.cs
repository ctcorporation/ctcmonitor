﻿namespace DashBoardData.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CustomerEmail : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Customer", "CU_EmailAddress", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Customer", "CU_EmailAddress");
        }
    }
}
