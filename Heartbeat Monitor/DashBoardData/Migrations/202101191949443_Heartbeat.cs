﻿namespace DashBoardData.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class Heartbeat : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Heartbeat",
                c => new
                {
                    HB_ID = c.Guid(nullable: false, identity: true, defaultValueSql: "newid()"),
                    HB_CU = c.Guid(nullable: false),
                    HB_AP = c.Guid(nullable: false),
                    HB_LastCommunicationDate = c.DateTime(nullable: false),
                    HB_LastOperation = c.DateTime(nullable: false),
                })
                .PrimaryKey(t => t.HB_ID);            

        }

        public override void Down()
        {
            DropTable("dbo.Heartbeat");
        }
    }
}
