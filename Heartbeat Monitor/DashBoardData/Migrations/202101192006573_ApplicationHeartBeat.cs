﻿namespace DashBoardData.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ApplicationHeartBeat : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Applications", "HB_ID", c => c.Guid(nullable: false));
            CreateIndex("dbo.Applications", "HB_ID");
            AddForeignKey("dbo.Applications", "HB_ID", "dbo.Heartbeat", "HB_ID", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Applications", "HB_ID", "dbo.Heartbeat");
            DropIndex("dbo.Applications", new[] { "HB_ID" });
            DropColumn("dbo.Applications", "HB_ID");
        }
    }
}
