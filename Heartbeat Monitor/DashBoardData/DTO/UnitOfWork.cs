﻿using DashBoardData.Models;
using DashBoardData.Respository;
using System;
using System.Data.Entity.Validation;

namespace DashBoardData.DTO
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly DashboardContext _context;
        private bool disposedValue;
        private string _errorLog;

        public UnitOfWork(DashboardContext context)
        {
            _context = context;
            AddRepos();

        }

        private void AddRepos()
        {
            Customers = new CustomerRepository(_context);
            Applications = new ApplicationRepository(_context);
            Heartbeats = new HeartbeatRepository(_context);
            ErrorLogs = new ErrorLogRepository(_context);
        }

        public ICustomerRepository Customers
        {
            get;
            private set;
        }
        public IApplicationRepository Applications
        {
            get;
            private set;
        }
        public IHeartbeatRepository Heartbeats
        {
            get;
            private set;
        }
        public IErrorLogRepository ErrorLogs
        {
            get;
            private set;
        }


        public bool DBExists()
        {
            bool exists = false;
            if (_context != null)
            {
                exists = _context.Database.Exists();
            }
            return exists;
        }

        public int Complete()
        {
            if (_context != null)
            {
                try
                {
                    return _context.SaveChanges();
                }
                catch (InvalidOperationException ex)
                {
                    _errorLog += ex.GetType().Name + "Found: Error was: " + ex.Message + ":" + ex.InnerException.Message + Environment.NewLine;
                }
                catch (DbEntityValidationException ex)
                {

                    foreach (DbEntityValidationResult validationResult in ex.EntityValidationErrors)
                    {
                        string entityName = validationResult.Entry.Entity.GetType().Name;
                        foreach (DbValidationError error in validationResult.ValidationErrors)
                        {
                            _errorLog += entityName + "." + error.PropertyName + ": " + error.ErrorMessage + Environment.NewLine;

                        }
                    }
                }
            }
            return -1;
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    _context.Dispose();
                }

                // TODO: free unmanaged resources (unmanaged objects) and override finalizer
                // TODO: set large fields to null
                disposedValue = true;
            }
        }

        // // TODO: override finalizer only if 'Dispose(bool disposing)' has code to free unmanaged resources
        // ~UnitOfWork()
        // {
        //     // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
        //     Dispose(disposing: false);
        // }

        public void Dispose()
        {
            // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
            Dispose(disposing: true);
            System.GC.SuppressFinalize(this);
        }
    }
}
