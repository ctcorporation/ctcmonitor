﻿using DashBoardData.Respository;
using System;

namespace DashBoardData.DTO
{
    public interface IUnitOfWork : IDisposable
    {
        IApplicationRepository Applications { get; }
        ICustomerRepository Customers { get; }
        IErrorLogRepository ErrorLogs { get; }
        IHeartbeatRepository Heartbeats { get; }
    }
}