﻿using DashBoardData.Models;

namespace DashBoardData.Respository
{
    public class HeartbeatRepository : GenericRepository<Heartbeat>, IHeartbeatRepository
    {
        public HeartbeatRepository(DashboardContext context)
            : base(context)
        {

        }

        public DashboardContext DashboardContext
        {
            get
            {
                return Context as DashboardContext;
            }
        }
    }
}
