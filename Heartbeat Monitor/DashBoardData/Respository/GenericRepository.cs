﻿using DashBoardData.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using System.Linq.Expressions;

namespace DashBoardData.Respository
{
    public class GenericRepository<TEntity> : IGenericRepository<TEntity> where TEntity : class
    {
        protected readonly DashboardContext Context;

        public GenericRepository(DashboardContext context)
        {
            Context = context;
        }

        public void Add(TEntity entity)
        {
            if (Context != null)
            {
                Context.Set<TEntity>().Add(entity);
            }
        }

        public void AddRange(IEnumerable<TEntity> entities)
        {
            if (Context != null)
            {
                Context.Set<TEntity>().AddRange(entities);
            }
        }

        public void Remove(TEntity entity)
        {
            if (Context != null)
            {
                Context.Set<TEntity>().Remove(entity);

            }
        }

        public void RemoveRange(IEnumerable<TEntity> entities)
        {
            if (Context != null)
            {
                Context.Set<TEntity>().RemoveRange(entities);
            }
        }

        public TEntity Get(Guid id)
        {
            TEntity found = null;
            if (Context != null)
            {
                Context.Set<TEntity>().Find(id);
            }
            return found;
        }

        public IQueryable<TEntity> Find(Expression<Func<TEntity, bool>> predicate)
        {
            IQueryable<TEntity> resultset = null;
            if (Context != null)
            {
                resultset = Context.Set<TEntity>().Where(predicate);
            }
            return resultset;
        }
        public IQueryable<TEntity> Find(string filter, object[] paramlist)
        {
            IQueryable<TEntity> resultset = null;
            if (Context != null)
            {
                resultset = Context.Set<TEntity>().Where(filter, paramlist);
            }
            return resultset;
        }

        public IQueryable<TEntity> GetAll()
        {
            IQueryable<TEntity> all = null;
            if (Context != null)
            {
                all = Context.Set<TEntity>();
            }
            return all;
        }

    }
}
