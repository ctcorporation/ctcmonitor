﻿using DashBoardData.Models;

namespace DashBoardData.Respository
{
    public class CustomerRepository : GenericRepository<Customer>, ICustomerRepository
    {
        public CustomerRepository(DashboardContext context)
        : base(context)
        {
        }

        public DashboardContext DashboardContext
        {
            get
            {
                return Context as DashboardContext;
            }
        }

    }
}
