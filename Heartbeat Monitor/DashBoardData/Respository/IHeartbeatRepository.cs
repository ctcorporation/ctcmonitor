﻿using DashBoardData.Models;

namespace DashBoardData.Respository
{
    public interface IHeartbeatRepository : IGenericRepository<Heartbeat>
    {
        DashboardContext DashboardContext { get; }
    }
}