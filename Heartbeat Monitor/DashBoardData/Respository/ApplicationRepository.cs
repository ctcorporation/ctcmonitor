﻿using DashBoardData.Models;

namespace DashBoardData.Respository
{
    public class ApplicationRepository : GenericRepository<CTCApplication>, IApplicationRepository
    {
        public ApplicationRepository(DashboardContext context)
            : base(context)
        {

        }

        public DashboardContext DashboardContext
        {
            get
            {
                return Context as DashboardContext;
            }
        }
    }
}
