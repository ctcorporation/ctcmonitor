﻿using DashBoardData.Models;

namespace DashBoardData.Respository
{
    public interface IErrorLogRepository : IGenericRepository<ErrorLog>
    {
        DashboardContext DashboardContext { get; }
    }
}