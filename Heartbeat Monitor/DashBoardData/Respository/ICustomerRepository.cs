﻿using DashBoardData.Models;

namespace DashBoardData.Respository
{
    public interface ICustomerRepository : IGenericRepository<Customer>
    {
        DashboardContext DashboardContext { get; }
    }
}