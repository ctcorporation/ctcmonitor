﻿using DashBoardData.Models;

namespace DashBoardData.Respository
{
    public class ErrorLogRepository : GenericRepository<ErrorLog>, IErrorLogRepository
    {
        public ErrorLogRepository(DashboardContext context)
            : base(context)
        {

        }

        public DashboardContext DashboardContext
        {
            get
            {
                return Context as DashboardContext;
            }
        }
    }
}
