﻿using DashBoardData.Models;

namespace DashBoardData.Respository
{
    public interface IApplicationRepository : IGenericRepository<CTCApplication>
    {
        DashboardContext DashboardContext { get; }
    }
}